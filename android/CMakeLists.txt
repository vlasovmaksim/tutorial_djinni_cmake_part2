cmake_minimum_required(VERSION 3.0)

set(PROJECT_NAME todolist)
project(${PROJECT_NAME})


file(GLOB SRCS 
  "../deps/djinni/support-lib/jni/*.cpp"
  "../generated-src/cpp/*.cpp"
  "../generated-src/jni/*.cpp"
  "../src/*.cpp"
  )

include_directories(
  "../deps/djinni/support-lib/jni/"
  "../deps/djinni/support-lib/"
  "../generated-src/cpp/"
  "../generated-src/jni/"
  "../src/"
  )


add_library(${PROJECT_NAME} SHARED ${SRCS})
