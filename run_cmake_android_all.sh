#! /usr/bin/env bash

# declare -a ABIs=("x86" "armeabi-v7a")
declare -a ABIs=("x86")

# declare -a BUILD_TYPES=("Debug" "Release")
declare -a BUILD_TYPES=("Debug")


rm -rf "build/android"
rm -rf "bin/android"

for ABI in "${ABIs[@]}"
do
    echo "$ABI"

    for BUILD_TYPE in "${BUILD_TYPES[@]}"
    do
    echo "$BUILD_TYPE"

        mkdir -p "build/android/$BUILD_TYPE/$ABI"

        cmake -G"MinGW Makefiles" \
         -DCMAKE_TOOLCHAIN_FILE="cmake/android.toolchain.cmake" \
         -DCMAKE_MAKE_PROGRAM="$ANDROID_NDK/prebuilt/windows-x86_64/bin/make.exe" \
         -DCMAKE_BUILD_TYPE=$BUILD_TYPE \
         -DANDROID_ABI=$ABI \
         -DANDROID_NATIVE_API_LEVEL=19 \
         -B"build/android/$BUILD_TYPE/$ABI" -H.
         
         cmake --build "build/android/$BUILD_TYPE/$ABI"

         echo "-----"
     done

     echo "----------"
done

echo "done"
