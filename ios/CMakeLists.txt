cmake_minimum_required(VERSION 3.0)

set(PROJECT_NAME todolist)
project(${PROJECT_NAME})


file(GLOB SRCS 
  "../deps/djinni/support-lib/objc/*.cpp"
  "../deps/djinni/support-lib/objc/*.mm"
  "../generated-src/cpp/*.cpp"
  "../generated-src/cpp/*.mm"
  "../generated-src/objc/*.cpp"
  "../generated-src/objc/*.mm"
  "../src/*.cpp"
  "../src/*.mm"
  )

include_directories(
  "../deps/djinni/support-lib/objc/"
  "../deps/djinni/support-lib/"
  "../generated-src/cpp/"
  "../generated-src/objc/"
  "../src/"
  )


add_library(${PROJECT_NAME} STATIC ${SRCS})
